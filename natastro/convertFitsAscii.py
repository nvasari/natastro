from astropy.io import fits, ascii
import numpy as np
import os
import sys

fitsFile = sys.argv[1]

# Defining input and output file names
##fitsFile = "MPA_gal_line_dr7_v5_2.fit"
fileName, fileExtension = os.path.splitext(fitsFile)
txtFile = fileName + ".txt"
smFile  = fileName + ".mac"

# Open fits file
hdulist = fits.open(fitsFile)

if (os.path.basename(fitsFile) == 'gal_info_dr7_v5_2.fit'):
    include_names = ["PLATEID", "MJD", "FIBERID", "RA", "DEC", "PRIMTARGET", "SECTARGET", "Z", "Z_ERR", "Z_WARNING", "V_DISP", "V_DISP_ERR", "SN_MEDIAN", "E_BV_SFD", "ZTWEAK", "ZTWEAK_ERR", "RELEASE"]
else:
    include_names = hdulist[1].columns.names
print hdulist[1].data[39810:39835]

# Save txtfile
#ascii.write(hdulist[1].data[39810:39835], sys.stdout, include_names = include_names)# uncomment this and comment below to run quick tests
ascii.write(hdulist[1].data, txtFile, Writer=ascii.CommentedHeader, delimiter=' ', include_names = include_names)

# Saving supermong macro to read txt file
SMtab = "        "
open(smFile, 'w').write('Read_%s\n\n' % (os.path.basename(fileName)))
open(smFile, 'a').write('%sdata %s\n' % (SMtab, (os.path.basename(txtFile))))
open(smFile, 'a').write("%sread {" % (SMtab))

for i, param in enumerate(include_names):
    open(smFile, 'a').write("%s%40s %3i \\\n" % (SMtab, param, i+1))

open(smFile, 'a').write("%s}\n\n" % (SMtab))

# EOF

