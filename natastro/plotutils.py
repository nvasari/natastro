import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
import time

#=========================================================================
def label_panels(fig, x = 0.05, y = 0.95, labels = None, addToLabel = None, va='top', **kwargs):
    import string
    if labels is None:
        labels = list(string.ascii_lowercase)
        labels = ['(%s)' % a for a in labels]
        
    if addToLabel is not None:
        labels = ['%s %s' % (a, b) for a, b in zip(labels, addToLabel)]

    for i, ax in enumerate(fig.get_axes()):
        ax.text(x, y, labels[i], transform=ax.transAxes, va = va, **kwargs)
#=========================================================================

#=========================================================================
def adjust_subplots(fig, desired_box_ratioN = 1., exclude_subp = []):    
    # http://stackoverflow.com/questions/14907062/matplotlib-aspect-ratio-in-subplots-with-various-y-axes
    for i, ax in enumerate(fig.get_axes(), start=1):
        if i not in exclude_subp:
            temp_inverse_axis_ratioN = abs( (ax.get_xlim()[1] - ax.get_xlim()[0])/(ax.get_ylim()[1] - ax.get_ylim()[0]) )
            ax.set(aspect = desired_box_ratioN * temp_inverse_axis_ratioN, adjustable='box-forced')
#=========================================================================

#=========================================================================
def title_date(title = None, y = 0.999):
    t = time.strftime("%d/%b/%Y %H:%M:%S")
    if title is None:
        suptitle = t
    else:
        suptitle = r'%s %s' % (title, t)
    
    plt.suptitle(suptitle, y = y)
#=========================================================================

#=========================================================================
def fix_ticks(axes = None, nx = 4, ny = 4, steps=[0, 2, 5, 10], **kwargs):

    if axes is not None:
        axes.minorticks_on()        
        axes.xaxis.set_major_locator( ticker.MaxNLocator(nbins = nx+1, steps=steps, **kwargs) )
        axes.yaxis.set_major_locator( ticker.MaxNLocator(nbins = ny+1, steps=steps, **kwargs) )
    else:
        print "@@> No axes given; nothing done to tickmarks."
#=========================================================================

#=========================================================================
def fix_colorbar_ticks(cb = None, ny = 4, **kwargs):

    if cb is None:
        cb = plt.colorbar()
        
    tick_locator = ticker.MaxNLocator(nbins = ny+1, **kwargs)
    cb.locator = tick_locator
    cb.update_ticks()        
#=========================================================================


#=========================================================================
def tight_colorbar(im, size = "7%", pad = "3%", zorder = 10, ax = None, **kwargs):
    if ax is None:
        ax = plt.gca()
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size, pad=pad, zorder = zorder)
    cbar = plt.colorbar(im, cax=cax, **kwargs)
    return cbar
#=========================================================================

    
#=========================================================================
def plot_identity(x, y, plot_fit = False, color='k', **kwargs):
    global_min = min(x.min(), y.min())
    global_max = max(x.max(), y.max())
    dx = global_max - global_min
    xmin = global_min - 0.1*dx
    xmax = global_max + 0.1*dx
        
    plt.plot([xmin, xmax], [xmin, xmax], color=color, **kwargs)
    plt.xlim(xmin, xmax)
    plt.ylim(xmin, xmax)

    if plot_fit:
        flag = (x > -999) & (y > -999)
        x = x[flag]
        y = y[flag]
        
        # Linear fit y(x) = a1 x + b1
        p = np.polyfit(x, y, 1)
        _x1 = np.array([xmin, xmax])
        a1, b1 = p[0], p[1]
        _y1 = a1 * _x1 + b1
        plt.plot(_x1, _y1, 'k:')

        # Linear fit x(y) = a2 y + b2
        p = np.polyfit(y, x, 1)
        _y2 = np.array([xmin, xmax])
        a2, b2 = p[0], p[1]
        _x2 = a2 * _y2 + b2
        plt.plot(_x2, _y2, 'k:')

        # Bisector fit: Formulae provided by Laerte 
        # (except that I use y = a x + b whereas y = a + b x for him)
        a2 = 1 / a2
        a = ( a1 * a2 - 1 + np.sqrt( (1 + a1**2) * ( 1 + a2**2) ) ) / (a1 + a2)
        xm = np.mean(x)
        ym = np.mean(y)
        b = ym - (a * xm)
        _x3 = np.array([xmin, xmax])
        _y3 = a * _x3 + b
        plt.plot(_x3, _y3, 'k--')
#=========================================================================

        
#=========================================================================
def save(fig, outFile, pdfFromEps = False, epsFromPdf = False, pdftocairo = False, **kwargs):

    if pdfFromEps:
        epsFile = os.path.splitext(outFile)[0] + '_tmp.eps'
        fig.savefig(epsFile, **kwargs)
        os.system("ps2pdf -dEPSCrop %s %s" % (epsFile, outFile))
        os.remove(epsFile)
    elif epsFromPdf:
        pdfFile = os.path.splitext(outFile)[0] + '_tmp.pdf'
        fig.savefig(pdfFile, **kwargs)
        os.system("pdftocairo -ps %s %s" % (pdfFile, outFile))
        os.remove(pdfFile)
    elif pdftocairo:
        tmpFile = os.path.splitext(outFile)[0] + '_tmp.pdf'
        fig.savefig(tmpFile, **kwargs)
        os.system("pdftocairo -pdf %s %s" % (tmpFile, outFile))
        os.remove(tmpFile)
    else:
        fig.savefig(outFile, **kwargs)
#=========================================================================

#=========================================================================
def save_figs_paper(fig, fileName, raster=True, dpi=300, savePDF = True, saveEPS = True,
                    pdftocairo = True, epsFromPdf = True):
    if raster == True:

        for ax in fig.get_axes():
            ax.set_rasterization_zorder(1)
        if savePDF:
            save(fig, '%s.pdf' % fileName, rasterized=True, dpi=dpi, pdftocairo=pdftocairo)
        if saveEPS:
            save(fig, '%s.eps' % fileName, rasterized=True, dpi=dpi, epsFromPdf=epsFromPdf)    
        
    else:
        
        for ax in fig.get_axes():
            ax.set_rasterization_zorder(-10)
        save(fig, '%s.pdf' % fileName, rasterized=False, dpi=dpi, pdftocairo=True)
#=========================================================================

#=========================================================================
def plotLatex(fn):
    def _plotLatex(override_params = {}, **kwargs):
        # Load normal setup
        plotSetup(**kwargs)

        # Add LaTeX configs
        params = { 
            'text.usetex': True,
            'text.latex.preamble': [
                r'\usepackage{amsmath}', 
                r'\usepackage{amssymb}', 
                r'\usepackage{siunitx}', 
                r'\sisetup{detect-all}',
            ],
        }
        plt.rcParams.update(params)
        
        # Load special function setup
        fn()

        # Load overridden paramters
        plt.rcParams.update(override_params)
        
    return _plotLatex
#=========================================================================

#=========================================================================
@plotLatex
def plotSetupTimes():
    pass
#=========================================================================

#=========================================================================
@plotLatex
def plotSetupMinion():
    params = { 
        'text.latex.preamble': [
            r'\usepackage[lf, mathtabular]{MinionPro}', 
            r'\usepackage{MnSymbol}',
        ]
    }
    plt.rcParams.update(params)

#plt.rcParams.update(params)
#=========================================================================

      
#=========================================================================
def plotSetup(fig_width_pt=448.07378, aspect=0.0, fontsize=None, lw=1.2, override_params={}):              
    # stolen from Andre...

    # From http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples
    inches_per_pt = 1.0 / 72.27
    if aspect == 0.0:
        golden_mean = (np.sqrt(5) - 1.0) / 2.0
        aspect = golden_mean
    fig_width = fig_width_pt * inches_per_pt
    fig_height = fig_width * aspect
    fig_size = (fig_width, fig_height)

    # Default fontsize
    if (fontsize is None):
        fontsize = int(0.08 * fig_width / inches_per_pt)
    
    params = {'backend': 'pdf',
              'interactive'  : True,

              'axes.labelsize': fontsize,
              'axes.titlesize': fontsize,
              'xtick.labelsize': fontsize,
              'ytick.labelsize': fontsize,
              'legend.fontsize': fontsize,

              'font.size': fontsize,
              'font.family': 'sans',
              'font.serif': ['Times New Roman', ],
              'font.sans-serif': ['Helvetica', ],
              'mathtext.fontset': 'custom',
              'text.usetex': False,

              'xtick.major.size': 6,
              'ytick.major.size': 6, 
              'xtick.minor.size': 3,	
              'ytick.minor.size': 3,

              'xtick.direction': 'in',
              'ytick.direction': 'in',
              'xtick.top':   True,
              'ytick.right': True,
              
              'figure.subplot.hspace': 0.2,
              'figure.subplot.wspace': 0.2,
              'figure.subplot.left':   0.125,
              'figure.subplot.right':  0.9,
              'figure.subplot.bottom': 0.1,
              'figure.subplot.top':    0.9,

              'figure.facecolor' : 'white' ,
              'figure.figsize'   : fig_size,

              'lines.linewidth'        : 1.0 * lw,    # line width in points
              'lines.markeredgewidth'  : 0.5 * lw,    # the line width around the marker symbol
              'patch.linewidth'        : 1.0 * lw,    # edge width in points
              'axes.linewidth'         : 1.0 * lw,    # edge linewidth
              'xtick.major.width'      : 0.5 * lw,    # major tick width in points
              'xtick.minor.width'      : 0.5 * lw,    # minor tick width in points
              'ytick.major.width'      : 0.5 * lw,    # major tick width in points
              'ytick.minor.width'      : 0.5 * lw,    # minor tick width in points
              'grid.linewidth'         : 0.5 * lw,    # in points

              'savefig.dpi': 300,
              }

    plt.rcdefaults()
    plt.rcParams.update(params)
    plt.rcParams.update(override_params)
#=========================================================================

    
#=========================================================================
def sample_plot():

    x1 = [1,2,3]
    y1 = [4,5,6]
    x2 = [1,2,3]
    y2 = [5,5,5]
    
    plt.clf()
    
    # @@@> Some usefulf tricks: line width, legend location, latex for label, fontsize
    fig311 = plt.subplot(3, 1, 1)
    plt.plot(x1, y1, label=r"bidu", ls="-", color='r', lw = 5)
    plt.plot(x1, y2, label=r"bugu", ls="-", color='y')
    plt.legend(loc = 2)
    plt.xlabel(r'H$\mathrm{H}\alpha^V_0$ [km s$^{-1}$]')
    plt.ylabel(r'$\langle \log \, t_\star \rangle_M$')
    plt.text(2, 5.5, r"an equation: $E=mc^2$", color = "b")
    
    # @@@> Tickmarks
    plt.minorticks_on()
    plt.xticks([1.1, 1.7, 2.3, 2.9])
    plt.yticks(np.arange(4.,6.1,0.5))
    
    # @@@> More tickmarks
    fig312 = plt.subplot(3, 1, 2)
    
    from matplotlib.ticker import MultipleLocator, FormatStrFormatter
    majorLocator   = MultipleLocator(20)
    majorFormatter = FormatStrFormatter('%d')
    minorLocator   = MultipleLocator(5)
    
    t = np.arange(0.0, 100.0, 0.1)
    s = np.sin(0.1*np.pi*t) * np.exp(-t*0.01)
    
    plt.plot(t,s)
    
    fig312.xaxis.set_major_locator(majorLocator)
    fig312.xaxis.set_major_formatter(majorFormatter)
    fig312.xaxis.set_minor_locator(minorLocator)
    
    
    fig313 = plt.subplot(3, 1, 3)
    
    
    # @@@> Adjust xgutter/ygutter
    plt.subplots_adjust(hspace=.5)
#=========================================================================
    
    
