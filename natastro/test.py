import os
import numpy as np
import matplotlib.pyplot as plt
import plotutils as nplot

xsize = 4

def plot(ifig = 1, name = '', axisFont = None):
    fig = plt.figure(1, figsize=(xsize, xsize))
    plt.clf()

    ax = fig.add_subplot(111)
    plt.plot(np.arange(10))
    plt.xlabel(r'Flux 1 2 H$\beta$')
    #plt.ylabel(r'Flux $\alpha$ [\si{\per\square\cm}]  [cm$^{-2}$]')
    plt.ylabel(r'Flux $\alpha$ [cm$^{-2}$]')

    if axisFont is not None:
        ax.set_xticklabels(ax.get_xticks(), axisFont)
        ax.set_yticklabels(ax.get_yticks(), axisFont)

    nplot.save(fig, 'fig1_%s.pdf' % name, dpi = 300, pdfFromEps = True)

    
nplot.plotSetupMinion(fig_width_pt=xsize*72.27, lw=3.)
plot(name = 'minion')



'''
import os

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import plotutils as nplot


xsize = 4

def plot(ifig = 1, name = '', axisFont = None):
    fig = plt.figure(1, figsize=(xsize, xsize))
    plt.clf()


    ax = fig.add_subplot(111)
    plt.plot(np.arange(10))
    plt.xlabel(r'Flux 1 2 H$\beta$')
    #plt.ylabel(r'Flux $\alpha$ [\si{\per\square\cm}]  [cm$^{-2}$]')
    plt.ylabel(r'Flux $\alpha$ [cm$^{-2}$]')

    if axisFont is not None:
        ax.set_xticklabels(ax.get_xticks(), axisFont)
        ax.set_yticklabels(ax.get_yticks(), axisFont)

        
    fig.set_tight_layout(True)
    fig.savefig('fig1_%s.pdf' % name, dpi = 300)
    fig.savefig('fig1_%s.eps' % name, dpi = 300)
    os.system("ps2pdf -dEPSCrop fig1_%s.eps fig1_%s.eps.pdf" % (name, name))



# Test Computer Modern
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': True})
mpl.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', 
    r'\usepackage{amssymb}', 
    r'\usepackage{siunitx}', 
    r'\sisetup{detect-all}',
]  

plot(name = 'cm')


# Test Times New Roman
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': True})
import matplotlib as mpl
mpl.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', 
    r'\usepackage{amssymb}', 
    r'\usepackage{siunitx}', 
    r'\sisetup{detect-all}',
    r'\renewcommand{\rmdefault}{ptm}',
    r'\usepackage{mathptmx} % rm & math', 
]  

plot(name = 'times')

# Test Minion
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': True})
mpl.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', 
    r'\usepackage{amssymb}', 
    r'\usepackage{siunitx}', 
    r'\sisetup{detect-all}',
    r'\usepackage[lf, mathtabular]{MinionPro}', 
    r'\usepackage{MnSymbol}',
]

plot(name = 'minion')



# Test Helvetica
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': True})
mpl.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', 
    r'\usepackage{amssymb}', 
    r'\usepackage{siunitx}', 
    r'\sisetup{detect-all}',
    r'\usepackage{helvet}       ',
    r'\usepackage[EULERGREEK]{sansmath}      ',
    r'\renewcommand\familydefault{\sfdefault}',
    r'\sansmath                              ',
    r'\sffamily                              ',
]


plot(name = 'helvetica')


# Test Hershey
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': False, 'font.family': 'sans-serif', 'mathtext.fontset': 'stix'})

import matplotlib.font_manager as fm
prop = fm.FontProperties(fname='/Users/natalia/Library/Fonts/RomanT.ttf')
mpl.rcParams['font.family'] = prop.get_name()
plot(name = 'hershey')



# Test stix
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': False} ) #, 'font.family': 'serif', 'mathtext.fontset': 'stix'})
plot(name = 'stix')

# Test stix
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': False, 'font.family': 'sans-serif', 'mathtext.fontset': 'stixsans'})
plot(name = 'stixsans')


# Test Palatino
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': True})
mpl.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', 
    r'\usepackage{amssymb}', 
    r'\usepackage{siunitx}', 
    r'\sisetup{detect-all}',
    r'\usepackage{mathpazo}',
]

plot(name = 'palatino')


# Test Gill Sans
nplot.plotSetup(fig_width_pt=xsize*72.27, lw = 3., override_params={'text.usetex': False, 'font.family': 'sans-serif', 'mathtext.fontset': 'stixsans'})

import matplotlib.font_manager as fm
prop = fm.FontProperties(family = 'Gill Sans', fname = '/Library/Fonts/GillSans.ttc')
mpl.rcParams['font.family'] = prop.get_name()
plot(name = 'gillsans')

'''
